# zfutils

Utilities for working with RFC 1035 zone files.

The `Makefile`, and most of the code was written on, and for OpenBSD.
However, if you are using a system with GNU make, it should default to using
the `GNUmakefile`.

**Be warned**: This is also an attempt for me to brush off my C programming
skills.

## Building the tools

None of these tools require dependencies beyond the C standard library.

```
## To use the system's default C compiler.
$ make clean all

## To use a different compiler.
$ CC=`which clang` make clean all
```

## Viewing the manual pages

If you wish to view the manual pages for each command, but they are not
installed into your system's man(1) directories, you can run:

```
$ groff -mdoc -Tascii man/<name>

## or ##

$ nroff -mdoc -Tascii man/<name>
```
