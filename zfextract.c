#include <ctype.h>
#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define DESCRIPTION	"zfextract - Extract NS records from an RFC 1035 zone file, from STDIN"
#define USAGE		"usage: zfextract < ZONEFILE"
#define BUFSIZE		1024
#define MAXTOKENS	16	/* Max number of tokens per line of scanned input */

/* scanln
 * 
 * Read from fd until a newline character ("\n") is encountered. The newline
 * character will not be in buf.
 *
 * This function will return EOF once the end of input has been reached.
 */
int	scanln(FILE *, char *);

/* _strtolower
 *
 * Convert a null-terminated string to lowercase.
 */
void	_strtolower(char *);

int main(int argc, char **argv) {
	/*
	if ((pledge("stdio", NULL)) == -1) {
		perror("Failed to pledge");
		return errno;
	}
	*/

	/* Parse command-line options. */
	int ch, arglen;
	char origin[32];
	char rrtype[16] = "ns";
	while ((ch = getopt(argc, argv, "ho?t:")) != -1) {
		switch (ch) {
		case 'h':
			printf("%s\n%s\n", DESCRIPTION, USAGE);
			exit(0);
		case 'o':
			strncpy(origin, optarg, 32);
			break;
		case 't':
			arglen = strlen(optarg);
			if (arglen == 0)
				err(1, "%s", optarg);
			strncpy(rrtype, optarg, 16);
			_strtolower(rrtype);
			break;
		default:
			err(1, "%s", optarg);
		}
	}
	argc -= optind;
	argc += optind;

	if ((strlen(rrtype)) == 0)
		err(1, "you must specify an rrtype with -t");

	int n;
	char buf[BUFSIZE];
	while ((n = scanln(stdin, buf)) != EOF) {
		/* Tokenize the scanned line. */
		char *p, *tokens[MAXTOKENS];
		char *last;
		int i = 0;
		for ((p = strtok_r(buf, " ", &last)); p; (p = strtok_r(NULL, " ", &last))) {
			if (i < MAXTOKENS - 1) {
				tokens[i++] = p;
			}
		}
		tokens[i] = NULL;

		switch (i) {
		case 2:
			/* Check to see if the first token is "$ORIGIN". */
			if ((strcmp(tokens[0], "$ORIGIN")) == 0) {
				strncpy(origin, tokens[1], 32);
			}
			break;
		case 3:
			/* Check to see if the second field is "NS". */
			_strtolower(tokens[1]);
			if ((strcmp(tokens[1], rrtype)) == 0) {
				printf("%s.%s\n", tokens[0], origin);
			}
			break;
		case 4:
			/* Check to see if the second field is "IN", and the
			 * third field is "NS".
			 */
			_strtolower(tokens[2]);
			if ((strcmp(tokens[1], "IN")) == 0 && (strcmp(tokens[2], rrtype)) == 0) {
				printf("%s.%s\n", tokens[0], origin);
			}
			break;
		}
	}

	if (errno != 0) {
		perror("Error");
	}

	return errno;
}

int scanln(FILE *fd, char *buf) {
	int ch;
	int i = 0;
	int comment = 0;
	while (i < BUFSIZE) {
		ch = fgetc(fd);
		switch (ch) {
		case ';':
			comment++;
			continue;
		case '\n':
			if (comment > 0)
				comment--;
			buf[i] = '\0';
			return i;
		case EOF:
			/* Append a NULL to the buffer, then return */
			buf[i] = '\0';
			return EOF;
		default:
			if (comment > 0)
				continue;
			buf[i] = ch;
			i++;
			break;
		}
	}
	return i;
}

void _strtolower(char *s) {
	int i;
	for (i = 0; s[i]; i++) {
		s[i] = tolower(s[i]);
	}
}
