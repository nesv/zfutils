CC	?= /usr/bin/cc
CFLAGS	= -Wall -Werror -Wextra \
	  -O2 -march=native -pipe \
	  -std=c99 -pedantic
PREFIX	?= /usr/local
BINDIR	:= ${PREFIX}/bin
MANDIR	:= ${PREFIX}/man

TARGETS	:= zfextract
MANUALS	= zfextract.1

all: ${TARGETS}

.PHONY: clean

clean:
	rm -rf ${TARGETS}
	rm -f *.o

zfextract: zfextract.o
	${CC} ${CFLAGS} -o $@ zfextract.o

.SUFFIXES: .c .o
.c.o:
	${CC} ${CFLAGS} -c $<

.PHONY: install install-targets install-manuals
install: install-targets install-manuals

install-targets: ${TARGETS}
.for tgt in ${TARGETS}
	install -m 0755 -S -s -u root -g bin ${tgt} ${BINDIR}
.endfor

install-manuals: ${MANUALS}
.for man in ${MANUALS}
	install -m 0644 -u root -g bin ${man} ${MANDIR}/man1
.endfor
